package com.begamer.card.json.element;

import com.begamer.card.cache.PlayerInfo;

public class ActivityDElement {

	public int needType;// 玩家兑换物品所要的物品类型 1.材料 2.装备 3.英雄卡 4.主动技能 5.被动技能 6.金币 8.钻石 9.符文 10.体力 
	public int needId;// 玩家兑换物品所要的物品id
	public int needNum;// 玩家兑换物品所要的物品数量
	public int curNeedNum;// 玩家当前拥有兑换物品所要的物品个数
	
	public void createActivityDElement(ActivityDElement ae, int sell,PlayerInfo pInfo,int needtype,int needid,int neednum,int id,int sole)
	{
		ae.setNeedType(needtype);
		ae.setNeedId(needid);
		ae.setNeedNum(neednum);
		int curNeedNum = 0;
		boolean r = false;
		switch (needtype) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			r = pInfo.checkExchangeNeedNum(needtype, needid, neednum);
			break;
		case 6:
			//金币
			curNeedNum = pInfo.player.getGold();
			break;
		case 8:
			//钻石
			curNeedNum = pInfo.player.getTotalCrystal();
			break;
		case 9:
			//符文
			curNeedNum = pInfo.player.getRuneNum();
			break;
		case 10:
			//体力
			curNeedNum = pInfo.player.getPower();
			break;
		}
		if (curNeedNum>=neednum || r)
		{
			sell = 1;
			String exnum = pInfo.player.getExchange();//27-1,
			if (exnum != null && exnum.trim().length()>0)
			{
				String []exchange = exnum.split(",");// 27-1
				for (int i = 0; i < exchange.length; i++)
				{//2
					String [] ex = exchange[i].split("-");//27 1
					int activityId=Integer.parseInt(ex[0]);
					int num = Integer.parseInt(ex[1]);
					if (activityId==id && sole==1 && num>=1)
					{
						sell = 0;
					}
				}
			}
		}
		else
		{
			sell = 3;
		}
		ae.setCurNeedNum(curNeedNum);
	}
	
	public int getNeedType()
	{
		return needType;
	}
	public void setNeedType(int needType)
	{
		this.needType = needType;
	}
	public int getNeedId()
	{
		return needId;
	}
	public void setNeedId(int needId)
	{
		this.needId = needId;
	}
	public int getNeedNum()
	{
		return needNum;
	}
	public void setNeedNum(int needNum)
	{
		this.needNum = needNum;
	}
	public int getCurNeedNum()
	{
		return curNeedNum;
	}
	public void setCurNeedNum(int curNeedNum)
	{
		this.curNeedNum = curNeedNum;
	}
}

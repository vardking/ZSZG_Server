package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;
import com.begamer.card.json.SCEGJson;
import com.begamer.card.json.SCPSJson;

public class SaveCGJson extends BasicJson {
	public int[] ics;// 角色卡索引
	public int[] iss;// 主动技能索引
	public SCPSJson[] ips;// 被动技能索引
	// public int[] ips;
	public SCEGJson[] ies;// 装备索引
	public int us;// 合体技id
	
	public int[] getIcs()
	{
		return ics;
	}
	
	public void setIcs(int[] ics)
	{
		this.ics = ics;
	}
	
	public int[] getIss()
	{
		return iss;
	}
	
	public void setIss(int[] iss)
	{
		this.iss = iss;
	}
	
	public SCPSJson[] getIps()
	{
		return ips;
	}
	
	public void setIps(SCPSJson[] ips)
	{
		this.ips = ips;
	}
	
	public SCEGJson[] getIes()
	{
		return ies;
	}
	
	public void setIes(SCEGJson[] ies)
	{
		this.ies = ies;
	}
	
	public int getUs()
	{
		return us;
	}
	
	public void setUs(int us)
	{
		this.us = us;
	}
	
}

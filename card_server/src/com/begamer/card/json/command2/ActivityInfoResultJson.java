package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.ActivityInfoExchangeElement;

public class ActivityInfoResultJson extends ErrorJson{

	public String name;
	public String content;
	public List<ActivityInfoExchangeElement> exActs;// 兑换型活动内容

	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public List<ActivityInfoExchangeElement> getExActs()
	{
		return exActs;
	}

	public void setExActs(List<ActivityInfoExchangeElement> exActs)
	{
		this.exActs = exActs;
	}

}

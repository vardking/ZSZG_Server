package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class FriendJson extends BasicJson
{
	/**0查看,1收体力,2送体力**/
	public int t;
	/**索引**/
	public int i;

	public int getT()
	{
		return t;
	}

	public void setT(int t)
	{
		this.t = t;
	}

	public int getI()
	{
		return i;
	}

	public void setI(int i)
	{
		this.i = i;
	}
	
}

package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.PkRankElement;

public class PkRankResultJson extends ErrorJson
{
	public List<PkRankElement> pes;//排名以及奖励信息

	public List<PkRankElement> getPes() {
		return pes;
	}

	public void setPes(List<PkRankElement> pes) {
		this.pes = pes;
	}
}

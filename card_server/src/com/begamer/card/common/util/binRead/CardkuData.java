package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.Random;

public class CardkuData implements PropertyReader{

	/**编号id**/
	public int id;
	/**卡库种类   1普通卡库,2必出卡库,3极品卡库**/
	public int kind;
	/**卡牌   id**/
	public int card;
	/**概率**/
	public int probability;
	/**首次跳转**/
	public int firstjump;
	/**二次跳转**/
	public int secondjump;
	
	private static HashMap<Integer, CardkuData> data = new HashMap<Integer, CardkuData>();
	private static List<CardkuData> dataList = new ArrayList<CardkuData>();
	private static int RandomSeed = 100;
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}
	@Override
	public void parse(String[] ss)
	{
		
	}
	@Override
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}
	
	public static CardkuData getCardkuData(int id)
	{
		return data.get(id);
	}
	public static List<CardkuData> getCardkuDatas()
	{
		return dataList;
	}
	/**卡库**/
	public static List<CardkuData> getCardkuDatas(int kind)
	{
		List<CardkuData> list = new ArrayList<CardkuData>();
		for (CardkuData ckd : dataList)
		{
			if (ckd.kind == kind)
			{
				list.add(ckd);
			}
		}
		return list;
	}
	/**
	 * 根据卡库类型获取cardId
	 */
	public static int getCardIdByKuType(int kind)
	{
		List<CardkuData> list=getCardkuDatas(kind);
		if(list!=null)
		{
			int roll=Random.getNumber(RandomSeed);
			for(CardkuData ckd:list)
			{
				if(roll<ckd.probability)
				{
					return ckd.card;
				}
				roll-=ckd.probability;
			}
		}
		return 0;
	}
	/**卡库跳转**/
	public static CardkuData getCardkuData2(int kind)
	{
		CardkuData ckd = null;
		for (CardkuData c : dataList)
		{
			if (c.kind == kind)
			{
				ckd = c;
				break;
			}
		}
		return ckd;
	}
}

package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpiritShopData implements PropertyReader
{
	public int id;
	public int number;
	public int viplevel;
	public int type;
	public int cost;
	
	private static HashMap<Integer, SpiritShopData> data =new HashMap<Integer, SpiritShopData>();
	private static List<SpiritShopData> dataList =new ArrayList<SpiritShopData>();
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	/**根据id获取data**/
	public static SpiritShopData getSpiritShopData(int index)
	{
		return data.get(index);
	}
	
	/**获取data长度**/
	public static Integer getDataSize()
	{
		return data.size();
	}
	
	/**根据vip获取可以购买的最大次数**/
	public static Integer getMaxSize(int viplv)
	{
		for(int i=0;i<dataList.size();i++)
		{
			if(dataList.get(i).viplevel>viplv)
			{
				return i+1;
			}
		}
		return -1;
	}
}

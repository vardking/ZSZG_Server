package com.begamer.card.model.view;

public class PublicMailView {
	// private String reward1;//格式:type&id&number
	private int type1;
	private int typeid1;
	private int number1;
	private int proNum1;
	// private String reward2;
	private int type2;
	private int typeid2;
	private int number2;
	private int proNum2;
	// private String reward3;
	private int type3;
	private int typeid3;
	private int number3;
	private int proNum3;

	private int type4;
	private int typeid4;
	private int number4;
	private int proNum4;

	private int type5;
	private int typeid5;
	private int number5;
	private int proNum5;

	private int type6;
	private int typeid6;
	private int number6;
	private int proNum6;
	// private String sendTime;//格式：12:10
	private int hour;
	private int minute;

	// private String deleteTime;//删除邮件时间,格式yyyy-MM-dd HH:mm

	/**显示邮件附带品实例化**/
	public PublicMailView(int type1, int typeid1, int number1, int type2,
			int typeid2, int number2, int type3, int typeid3, int number3,
			int type4, int typeid4, int number4, int type5, int typeid5,
			int number5, int type6, int typeid6, int number6, int hour,
			int minute)
	{
		this.type1 = type1;
		this.typeid1 = typeid1;
		this.number1 = number1;
		this.type2 = type2;
		this.typeid2 = typeid2;
		this.number2 = number2;
		this.type3 = type3;
		this.typeid3 = typeid3;
		this.number3 = number3;
		this.type4 = type4;
		this.typeid4 = typeid4;
		this.number4 = number4;
		this.type5 = type5;
		this.typeid5 = typeid5;
		this.number5 = number5;
		this.type6 = type6;
		this.typeid6 = typeid6;
		this.number6 = number6;
		this.hour = hour;
		this.minute = minute;
	}
	/**显示全局掉落物品实例化**/
	public PublicMailView(int type1, int typeid1, int number1, int proNum1,int type2,
			int typeid2, int number2, int proNum2,int type3, int typeid3, int number3,int proNum3,
			int type4, int typeid4, int number4, int proNum4,int type5, int typeid5,
			int number5, int proNum5,int type6, int typeid6, int number6, int proNum6)
	{
		this.type1 = type1;
		this.typeid1 = typeid1;
		this.number1 = number1;
		this.proNum1 = proNum1;
		this.type2 = type2;
		this.typeid2 = typeid2;
		this.number2 = number2;
		this.proNum2 = proNum2;
		this.type3 = type3;
		this.typeid3 = typeid3;
		this.number3 = number3;
		this.proNum3 = proNum3;
		this.type4 = type4;
		this.typeid4 = typeid4;
		this.number4 = number4;
		this.proNum4 = proNum4;
		this.type5 = type5;
		this.typeid5 = typeid5;
		this.number5 = number5;
		this.proNum5 = proNum5;
		this.type6 = type6;
		this.typeid6 = typeid6;
		this.number6 = number6;
		this.proNum6 = proNum6;
	}

	public int getHour()
	{
		return hour;
	}

	public void setHour(int hour)
	{
		this.hour = hour;
	}

	public int getMinute()
	{
		return minute;
	}

	public void setMinute(int minute)
	{
		this.minute = minute;
	}

	public int getType1()
	{
		return type1;
	}

	public void setType1(int type1)
	{
		this.type1 = type1;
	}

	public int getTypeid1()
	{
		return typeid1;
	}

	public void setTypeid1(int typeid1)
	{
		this.typeid1 = typeid1;
	}

	public int getNumber1()
	{
		return number1;
	}

	public void setNumber1(int number1)
	{
		this.number1 = number1;
	}

	public int getType2()
	{
		return type2;
	}

	public void setType2(int type2)
	{
		this.type2 = type2;
	}

	public int getTypeid2()
	{
		return typeid2;
	}

	public void setTypeid2(int typeid2)
	{
		this.typeid2 = typeid2;
	}

	public int getNumber2()
	{
		return number2;
	}

	public void setNumber2(int number2)
	{
		this.number2 = number2;
	}

	public int getType3()
	{
		return type3;
	}

	public void setType3(int type3)
	{
		this.type3 = type3;
	}

	public int getTypeid3()
	{
		return typeid3;
	}

	public void setTypeid3(int typeid3)
	{
		this.typeid3 = typeid3;
	}

	public int getNumber3()
	{
		return number3;
	}

	public void setNumber3(int number3)
	{
		this.number3 = number3;
	}

	public int getType4()
	{
		return type4;
	}

	public void setType4(int type4)
	{
		this.type4 = type4;
	}

	public int getTypeid4()
	{
		return typeid4;
	}

	public void setTypeid4(int typeid4)
	{
		this.typeid4 = typeid4;
	}

	public int getNumber4()
	{
		return number4;
	}

	public void setNumber4(int number4)
	{
		this.number4 = number4;
	}

	public int getType5()
	{
		return type5;
	}

	public void setType5(int type5)
	{
		this.type5 = type5;
	}

	public int getTypeid5()
	{
		return typeid5;
	}

	public void setTypeid5(int typeid5)
	{
		this.typeid5 = typeid5;
	}

	public int getNumber5()
	{
		return number5;
	}

	public void setNumber5(int number5)
	{
		this.number5 = number5;
	}

	public int getType6()
	{
		return type6;
	}

	public void setType6(int type6)
	{
		this.type6 = type6;
	}

	public int getTypeid6()
	{
		return typeid6;
	}

	public void setTypeid6(int typeid6)
	{
		this.typeid6 = typeid6;
	}

	public int getNumber6()
	{
		return number6;
	}

	public void setNumber6(int number6)
	{
		this.number6 = number6;
	}

	public int getProNum1()
	{
		return proNum1;
	}

	public void setProNum1(int proNum1)
	{
		this.proNum1 = proNum1;
	}

	public int getProNum2()
	{
		return proNum2;
	}

	public void setProNum2(int proNum2)
	{
		this.proNum2 = proNum2;
	}

	public int getProNum3()
	{
		return proNum3;
	}

	public void setProNum3(int proNum3)
	{
		this.proNum3 = proNum3;
	}

	public int getProNum4()
	{
		return proNum4;
	}

	public void setProNum4(int proNum4)
	{
		this.proNum4 = proNum4;
	}

	public int getProNum5()
	{
		return proNum5;
	}

	public void setProNum5(int proNum5)
	{
		this.proNum5 = proNum5;
	}

	public int getProNum6()
	{
		return proNum6;
	}

	public void setProNum6(int proNum6)
	{
		this.proNum6 = proNum6;
	}

}

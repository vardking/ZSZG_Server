package com.begamer.card.model.pojo;


public class Equip {
	private int id;
	private int playerId;
	private int equipId;
	private int level;
	private String createTime;
	private int sell;
	private int newType;
	
	public static Equip createEquip(int playerId,int equipId)
	{
		Equip e=new Equip();
		e.setEquipId(equipId);
		e.setPlayerId(playerId);
		e.setLevel(1);
		e.setCreateTime(System.currentTimeMillis()+"");
		e.setSell(0);
		e.setNewType(0);
		return e;
	}
	
	public Equip(){
		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPlayerId() {
		return playerId;
	}
	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}
	public Integer getEquipId() {
		return equipId;
	}
	public void setEquipId(Integer equipId) {
		this.equipId = equipId;
	}
	public Integer getLevel()
	{
		return level;
	}
	public void setLevel(Integer level)
	{
		this.level = level;
	}
	public String getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(String createTime)
	{
		this.createTime = createTime;
	}

	public Integer getSell() {
		return sell;
	}

	public void setSell(Integer sell) {
		this.sell = sell;
	}

	public int getNewType() {
		return newType;
	}

	public void setNewType(int newType) {
		this.newType = newType;
	}
	
}

package com.begamer.card.model.pojo;

public class Friend {
	private int id;
	private int playerId;
	private int friendId;
	/**好友状态:0未赠不可收,1已删除,2未赠可收,3未赠已收,4已赠不可收,5已赠可收,6已赠已收
	 * 申请状态:0已经是好友,1未申请,2已申请**/
	private int state;
	private String lastHelpDay;
	private String addDate;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	public int getFriendId()
	{
		return friendId;
	}
	public void setFriendId(int friendId)
	{
		this.friendId = friendId;
	}
	public int getState()
	{
		return state;
	}
	public void setState(int state)
	{
		this.state = state;
	}
	public String getLastHelpDay()
	{
		return lastHelpDay;
	}
	public void setLastHelpDay(String lastHelpDay)
	{
		this.lastHelpDay = lastHelpDay;
	}
	public void setAddDate(String addDate)
	{
		this.addDate = addDate;
	}
	public String getAddDate()
	{
		return addDate;
	}

}

package com.begamer.card.model.pojo;

public class Var
{
	public int id;
	public int var;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getVar()
	{
		return var;
	}
	public void setVar(int var)
	{
		this.var = var;
	}
}

package com.begamer.card.common.util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @ClassName: DirUtil
 * @Description: TODO
 * @author gs
 * @date Nov 10, 2011 3:46:27 PM
 * 
 */
public class DirUtil {

	/**
	 * 返回以当前年月日构成的文件夹目录结构，并且创建目录，如：/200912/01/
	 */

	public static String getMiddlePath(String basePath) {
		Date now = new Date();
		DateFormat f = new SimpleDateFormat("yyyyMM");
		String yearMonth = f.format(now);
		String day = new Integer(now.getDate()).toString();
		String middlePath = "/" + yearMonth + "/" + day + "/";
		createDir(basePath + "/" + yearMonth + "/");
		createDir(basePath + middlePath);
		String second = new Long(new Date().getTime()).toString();
		createDir(basePath + middlePath + second + "/");

		return middlePath + second + "/";
	}

	/**
	 * 
	 * @Title: createDir
	 * @Description: TODO
	 */
	private static void createDir(String dir) {
		File d = new File(dir);
		if (!d.exists())
			d.mkdir();
	}

	/**
	 * 
	 * @Title: getMiddlePathYearMonthDay
	 * @Description: TODO
	 */
	public static String getMiddlePathYearMonthDay(String basePath) {
		Date now = new Date();
		DateFormat f = new SimpleDateFormat("yyyyMM");
		String yearMonth = f.format(now);
		String day = new Integer(now.getDate()).toString();
		String middlePath = "/" + yearMonth + "/" + day + "/";
		createDir(basePath + "/" + yearMonth + "/");
		createDir(basePath + middlePath);
		return middlePath;
	}
}

package com.begamer.card.json;

public class PayExtraJson
{
	public String username;
	public int playerId;
	public int rechargeId;
	public String os;
	
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public int getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	public int getRechargeId()
	{
		return rechargeId;
	}
	public void setRechargeId(int rechargeId)
	{
		this.rechargeId = rechargeId;
	}
	public String getOs()
	{
		return os;
	}
	public void setOs(String os)
	{
		this.os = os;
	}
}

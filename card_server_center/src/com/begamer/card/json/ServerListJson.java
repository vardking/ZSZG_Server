package com.begamer.card.json;

import java.util.List;

public class ServerListJson
{
	/**元素格式:id-name-ip-port-type-state**/
	public List<String> list;
	public String announce;//==公告==//
	
	public List<String> getList()
	{
		return list;
	}

	public void setList(List<String> list)
	{
		this.list = list;
	}

	public String getAnnounce()
	{
		return announce;
	}

	public void setAnnounce(String announce)
	{
		this.announce = announce;
	}

}

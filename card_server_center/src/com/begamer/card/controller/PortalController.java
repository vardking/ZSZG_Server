/**  
 * @Title: PortalController.java
 * @Package com.bw30.bwr.controller
 * @Description: TODO(用一句话描述该文件做什么)
 * @author 国实  
 * @date Apr 6, 2010 3:34:19 PM
 * @version V1.0  
 */
package com.begamer.card.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.Constant;
import com.begamer.card.log.ManagerLogger;
import com.begamer.card.model.dao.ManagerLoginDao;
import com.begamer.card.model.pojo.Manager;

/**
 * 
 * @ClassName: PortalController
 * @author 国实
 * @date Feb 23, 2011 11:12:28 AM
 * 
 */
public class PortalController extends AbstractMultiActionController {

	private Logger logger = Logger.getLogger(PortalController.class);
	private static Logger managerLogger = ManagerLogger.logger;

	@Autowired
	private ManagerLoginDao managerLoginDao;

	public ModelAndView welcome(HttpServletRequest request,
			HttpServletResponse response)
	{
		return new ModelAndView("welcome.jsp");
	}
	
	/**
	 * 
	 * @Title: index
	 * @Description:登陆页
	 */
	public ModelAndView index(HttpServletRequest request,
			HttpServletResponse response)
	{
		return new ModelAndView("/portal/login.vm");
	}

	public ModelAndView error(HttpServletRequest request,
			HttpServletResponse response)
	{
		return new ModelAndView("/error.vm");
	}

	/**
	 * 
	 * @Title: login
	 * @Description:登录动作
	 */
	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response)
	{
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String rand = request.getParameter("rand");
		Manager manager = managerLoginDao.login(name, password);
		if (null != request.getSession().getAttribute(Constant.IMAGE_SESSION) && rand != null && rand.equals((String) request.getSession().getAttribute(Constant.IMAGE_SESSION)))
		{

		}
		else
		{
			request.setAttribute("showmsg", "验证码错误,请重试！");
			return index(request, response);
		}
		if (manager != null)
		{
			request.getSession().setAttribute(Constant.LOGIN_USER, manager);
			request.setAttribute(Constant.LOGIN_USER, manager);
			logger.info("用户" + manager.getName() + "登录成功！" + "登录IP" + request.getRemoteAddr());
			managerLogger.info(manager.getName() + "|用户" + manager.getName() + "登录成功！" + "|登录IP" + request.getRemoteAddr());
		}
		else
		{
			request.setAttribute("showmsg", "用户名密码错误,请重试！");
			return index(request, response);
		}
		
		return new ModelAndView("/portal/portal.vm");
	}

	/**
	 * 
	 * @Title: logout
	 * @Description: 登出
	 */
	public ModelAndView logout(HttpServletRequest request,
			HttpServletResponse response)
	{
		try
		{
			request.getSession().removeAttribute(Constant.LOGIN_USER);
			return new ModelAndView("/portal/login.vm");
		}
		catch (Exception e)
		{
			logger.error(e.toString());
			return new ModelAndView("error.vm");
		}
	}

	/**
	 * 
	 * @Title: logout
	 * @Description:显示左边菜单
	 */
	public ModelAndView left(HttpServletRequest request,
			HttpServletResponse response)
	{
		try
		{
			return new ModelAndView("/portal/left.vm");
		}
		catch (Exception e)
		{
			logger.error(e.toString());
			return new ModelAndView("error.vm");
		}
	}
	public ModelAndView main(HttpServletRequest request,
			HttpServletResponse response)
	{

		return new ModelAndView("/portal/main.vm");
	}

	public ModelAndView top(HttpServletRequest request,
			HttpServletResponse response)
	{
		Manager manager = (Manager) request.getSession().getAttribute(
				Constant.LOGIN_USER);
		request.setAttribute("manager", manager);
		request.setAttribute("version", Cache.getInstance().getVersion());
		return new ModelAndView("/portal/top.vm");
	}

	/**
	 * 
	 * @Title: rand
	 * @Description: 生成验证码图片
	 */
	public ModelAndView rand(HttpServletRequest request,
			HttpServletResponse response)
	{
		// 设置页面不缓存
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		// 在内存中创建图象
		int width = 60, height = 20;
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);

		// 获取图形上下文
		Graphics g = image.getGraphics();

		// 生成随机类
		Random random = new Random();

		// 设定背景色
		g.setColor(getRandColor(200, 250));
		g.fillRect(0, 0, width, height);

		// 设定字体
		g.setFont(new Font("Times New Roman", Font.PLAIN, 18));

		// 画边框
		// g.setColor(new Color());
		// g.drawRect(0,0,width-1,height-1);

		// 随机产生155条干扰线，使图象中的认证码不易被其它程序探测到
		g.setColor(getRandColor(160, 200));
		for (int i = 0; i < 155; i++)
		{
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			g.drawLine(x, y, x + xl, y + yl);
		}

		// 取随机产生的认证码(4位数字)
		String sRand = "";
		for (int i = 0; i < 4; i++)
		{
			String rand = String.valueOf(random.nextInt(10));
			sRand += rand;
			// 将认证码显示到图象中
			g.setColor(new Color(20 + random.nextInt(110), 20 + random
					.nextInt(110), 20 + random.nextInt(110)));// 调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
			g.drawString(rand, 13 * i + 6, 16);
		}

		// 将认证码存入SESSION
		request.getSession().setAttribute(Constant.IMAGE_SESSION, sRand);
		// 图象生效
		g.dispose();
		OutputStream output;
		try
		{
			output = response.getOutputStream();
			// 输出图象到页面
			ImageIO.write(image, "JPEG", output);
			output.flush();
		}
		catch (IOException e)
		{
			logger.error(e.toString());
		}
		return null;
	}

	private Color getRandColor(int fc, int bc)
	{// 给定范围获得随机颜色
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}
}

package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SkillData implements PropertyReader
{
	public int index;
	public int number;
	public String name;
	public int exptype;
	public int type;
	public int star;
	public int level;
	public int exp;
	public int atkTarget;
	public int element;
	public int view;
	public float viewNum;
	public String description;
	public String upgradetext;
	public int sell;
	public float atkNumbers;
	public String icon;
	public String icon2;
	//==释放音效==//
	public String music;
	//==蓄力音效==//
	public String chargemusic;
	public int atkAction;
	public int position;
	public int background;
	public int atkCharge;
	public String atkChargeSE;
	public float atkChargeSETime;
	public float atkChargeENDTime;
	public int atkChargeSEPositionType;
	public int spawn;
	public String spawnID;
	public float spawnDelayTime;
	public float spawnTime;
	public float spawnENDTime;
	public int spawnPositionType;
	public String hurtSE;
	//==受伤音效==//
	public String hurtMusic;
	public float hurtSETime;
	public float hurtENDTime;
	public int hurtSEPositionType;
	public int defTarget;
	public float numberX;
	public float numberY;
	public float numberZ;
	public int defAction;
	public String defActionSE;
	public float defActionSETime;
	public float defActionENDTime;
	public int defActionSEPositionType;
	public String defTargetSE;
	public float defTargetSETime;
	public float defTargetENDTime;
	public int defTargetSEPositionType;
	public String defSE;
	public float defSETime;
	public float defENDTime;
	public int defSEPositionType;
	public int healTarget;
	public float healNumbers;
	public int healAction;
	public String healActionSE;
	public float healActionSETime;
	public float healActionENDTime;
	public int healActionSEPositionType;
	public int healCharge;
	public String healChargeSE;
	public float healChargeSETime;
	public float healChargeENDTime;
	public int healChargeSEPositionType;
	public String healedSE;
	public float healedSETime;
	public float healedENDTime;
	public int healedSEPositionType;
	

	private static HashMap<Integer, SkillData> data=new HashMap<Integer, SkillData>();
	private static List<SkillData> dataList=new ArrayList<SkillData>();
	
	@Override
	public void addData()
	{
		data.put(index,this);
		dataList.add(this);
	}
	
	public static SkillData getData(int skillId)
	{
		return data.get(skillId);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	/** 所有主动技能* */
	public static List<SkillData> getSkillDatas()
	{
		return dataList;
	}
}
